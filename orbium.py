import numpy as np
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from scipy.signal import convolve2d

from data import orbium
from tools import gauss

size = 64  # Taille de la grille
T = 10  # Pas de temps
R = 13  # Rayon du noyau

# on replie la grille de 0
A = np.zeros((size, size))

# on place orbium au centre
A[20:20 + len(orbium), 20:20 + len(orbium)] = orbium

# Création du noyau
y, x = np.ogrid[-R:R, -R:R]
distance = np.sqrt((1 + x) ** 2 + (1 + y) ** 2) / R

mu = 0.5
sigma = 0.15
K_lenia = gauss(distance, mu, sigma)
K_lenia[distance > 1] = 0  # Cut at d=1
K = K_lenia / np.sum(K_lenia)  # Normalize


def growth(U):
    """ smooth growth function """
    m = 0.15  # Moyenne
    s = 0.015  # Écart-type
    # * 2 - 1 permet de normaliser la fonction de croissance entre -1 et 1
    return gauss(U, m, s) * 2 - 1


def update(i):
    global A, img

    # Convolution de la grille
    # le mode `same` permet de garder la taille de la grille
    U = convolve2d(A, K, mode='same', boundary='wrap')

    # Mise à jour de la grille
    # 1 / T * growth(U) permet de normaliser la croissance par le pas de temps
    A = A + 1 / T * growth(U)

    # On s'assure que les valeurs restent dans [0, 1]
    A = np.clip(A, 0, 1)
    img.set_array(A)
    return img,


fig = plt.figure()
img = plt.imshow(A, cmap='viridis', interpolation="nearest", vmin=0)
plt.title("Lenia te voila !")
ani = FuncAnimation(fig, update, frames=200, interval=20, blit=True)
plt.show()
