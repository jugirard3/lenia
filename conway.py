import matplotlib.animation
import matplotlib.pylab as plt
import numpy as np
import scipy.signal

size = 64
np.random.seed(0)
A = np.random.randint(2, size=(size, size))

K = np.array([[1, 1, 1],
              [1, 0, 1],
              [1, 1, 1]])


def update(i):
    global A
    # Somme des voisins
    U = scipy.signal.convolve2d(A, K, mode='same', boundary='wrap')
    A = np.where((U < 2) | (U > 3), 0, A)  # sous population ou surpopulation → mort
    A = np.where((U == 3), 1, A)  # reproduction
    img.set_array(A)
    return img


fig, ax = plt.subplots()
img = ax.imshow(A, cmap='gray', interpolation='none')
ani = matplotlib.animation.FuncAnimation(fig, update, frames=100, interval=100)
plt.show()
