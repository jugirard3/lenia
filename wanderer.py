import numpy as np
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from scipy.signal import convolve2d

from data import wanderer
from tools import gauss

size = 64  # Taille de la grille
T = 10  # Pas de temps
R = 13  # Rayon du noyau

# on replie la grille de 0
A = np.zeros((size, size))

# on place wanderer au centre
A[20:20 + len(wanderer), 0:0 + len(wanderer[0])] = wanderer

b = np.asarray([3 / 4, 1])

# Création du noyau
y, x = np.ogrid[-R:R, -R:R]
distance = np.sqrt((1 + x) ** 2 + (1 + y) ** 2) / R * len(b)

mu = 0.5
sigma = 0.15

K_lenia = (distance < len(b)) * b[np.minimum(distance.astype(int), len(b) - 1)] * gauss(distance % 1, mu, sigma)
K = K_lenia / np.sum(K_lenia)  # Normalize


def growth(U):
    """ smooth growth function """
    m = 0.1  # Moyenne
    s = 0.005  # Écart-type
    return gauss(U, m, s)


def update(i):
    global A, img

    # Convolution de la grille
    # le mode `same` permet de garder la taille de la grille
    U = convolve2d(A, K, mode='same', boundary='wrap')

    # Mise à jour de la grille
    A = A + 1 / T * (growth(U) - A)

    # On s'assure que les valeurs restent dans [0, 1]
    A = np.clip(A, 0, 1)
    img.set_array(A)
    return img,


fig = plt.figure()
img = plt.imshow(A, cmap='viridis', interpolation="nearest", vmin=0)
ani = FuncAnimation(fig, update, frames=200, interval=20, blit=True)
plt.show()
