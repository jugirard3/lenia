import matplotlib.pyplot as plt
import numpy as np


def gauss(x: float, m: float, s: float) -> float:
    """ Fonction gaussienne
    :param x: valeur à évaluer
    :param m: moyenne
    :param s: écart-type
    :return: valeur de la gaussienne
    """
    return np.exp(-((x - m) / s) ** 2 / 2)


def afficher_kenel(kernel: np.ndarray) -> None:
    """ Affiche le noyau de convolution """
    plt.figure()  # Création d'une nouvelle figure
    plt.imshow(kernel, cmap="viridis")
    print(kernel)


def afficher_gauss(m: float, s: float) -> None:
    """ Affiche une gaussienne de moyenne m et d'écart-type s. La gaussienne est normalisée entre -1 et 1. """
    plt.figure()
    x = np.linspace(-1, 1, 100)
    y = gauss(x, m, s) * 2 - 1
    plt.plot(x, y)
    plt.title(f"Gaussienne de moyenne {m} et d'écart-type {s}")
    plt.show()
