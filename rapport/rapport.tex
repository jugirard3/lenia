\documentclass{article}
\usepackage{graphicx}
\usepackage{float}


\begin{document}

    \title{Lenia}
    \author{MAROTTE Dimitry,GIRARD Jules}
    \date{2024}

    \maketitle


    \section{Introduction}\label{sec:introduction}

    Lenia est une amélioration du jeu de Conway, le jeu de la vie, inventé par John Horton Conway en 1970.
    Lenia a été inventé par Bert Wang-Chak Chan en 2018.
    Basé sur des automates cellulaires, il est plus complexe que le jeu de la vie.
    Il est composé de cellules qui évoluent en fonction de leur environnement, en utilisant une fonction de croissance et d'un kernel (un noyau de convolution).
    Il est possible de modifier le kernel, la grille et la fonction de croissance.
    Différentes formes et évolution peuvent être obtenues en modifiant ces paramètres.
    Beaucoup de formes sont déjà connues et ont été nommées par les utilisateurs de Lenia.
    Nous allons étudier certaines de ses formes.


    \begin{figure}[H]
        \centering
        \includegraphics[width=0.5\textwidth]{assets/conway}
        \caption{Jeu de la vie, de John Horton Conway}
        \label{fig:jdlv}
    \end{figure}


    \section{Kernel}\label{sec:kernel}

    Le kernel est un noyau de convolution qui permet de modifier la grille à chaque étape de l'évolution.
    Il est composé d'une matrice de coéfficients qui sont multipliées par les valeurs de la grille pour obtenir une nouvelle valeur pour chaque cellule de la grille.
    Il est possible de modifier le kernel pour obtenir des formes ou une évolution différente.
    Dans notre cas, nous avons utilisé un kernel gaussien.
    Les kernels gaussiens sont caractérisés par :
    \begin{itemize}
        \item la taille du noyau,
        \item $\mu$ la moyenne,
        \item $\sigma$ l'écart-type.
        \item la normalisation du noyau.
    \end{itemize}

    Dans notre exemple, la taille du noyau est de 26x26, $\mu$ est de 0,5 et $\sigma$ est de 0,15.

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.5\textwidth]{assets/kernel}
        \caption{Kernel gaussien}
        \label{fig:kernel}
    \end{figure}

    À chaque étape de l'évolution, le kernel est appliqué à la grille.

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.5\textwidth]{assets/convolution}
        \caption{Convolution entre le kernel et la grille}
        \label{fig:convolution}
    \end{figure}

    Une fois la convolution effectuée, on obtient une nouvelle grille qui contient la somme pondérée des cellules voisines pour chaque cellule de la grille.
    Cette nouvelle grille est ensuite modifiée en fonction de la fonction de croissance.

    \subsection{Kernel multi-anneaux}\label{subsec:kernel-multi-anneau}

    Nous pouvons ensuite étendre ce kernel afin d'obtenir plusieurs anneaux plutôt qu'un seul.
    Pour cela, nous allons utiliser la même idée, mais en ajoutant un nouveau paramètre que l'on va appeler b.
    Ce paramètre va permettre d'ajuster la portée du kernel.
    On sélectionnera ensuite les poids du kernel depuis b basé sur la distance entre la cellule et le centre du kernel.
    Ceci créera donc un kernel avec plusieurs anneaux (kernel en anneaux concentriques)

    Voici un exemple avec b = [3/4,1] utiliser pour la forme Wanderer.

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.5\textwidth]{assets/kernel_multi_anneau}
        \caption{Kernel multi-anneau}
        \label{fig:kernel_multi_anneau}
    \end{figure}

    Voici un autre exemple avec b = [0.5,1,0.667] utiliser pour la forme Geminium:

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.5\textwidth]{assets/kernel_multi_anneau2}
        \caption{Kernel multi-anneau}
        \label{fig:kernel_multi_anneau2}
    \end{figure}


    \section{Fonction de croissance}\label{sec:fonction-de-croissance}

    La fonction de croissance indique comment les cellules de la grille vont évoluer en fonction de leur environnement.
    Par exemple, une cellule peut avoir une valeur plus élevée si elle est entourée de cellules ayant une valeur élevée.
    Dans le cas d'une fonction de croissance gaussienne, les cellules vont évoluer en fonction de la distance avec les cellules voisines et de leur valeur.
    Ici, la distance est calculée à l'aide de la convolution entre le kernel et la grille (voir figure~\ref{fig:convolution}).

    Pour que les cellules évoluent de manière cohérente, la fonction de croissance doit être normalisée par le pas de temps et dans l'intervalle [-1, 1].

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.5\textwidth]{assets/fonction_croissance}
        \caption{Fonction de croissance gaussienne}
        \label{fig:fonction_croissance}
    \end{figure}

    On voit sur la fonction de croissance gaussienne un pic central autour de 0.15.
    Cela indique que si la somme pondérée des cellules voisines est proche de 0.15, la cellule va évoluer positivement.
    Dans le cas contraire, la cellule va évoluer négativement.

    \subsection{Grille}\label{subsec:grille}
    La grille est une matrice à deux dimensions qui contient les valeurs des cellules.
    Chaque cellule est représentée par un nombre réel compris entre 0 et 1.


    \section{Formes particulières}\label{sec:formes-particulieres}

    Lorsque l'on choisie des paramètres spécifiques, on peut obtenir des formes particulières qui persistent dans le temps et peuvent se déplacer.

    \subsection{Orbium}\label{subsec:orbium}

    La forme Orbium est une forme circulaire qui se déplace en ligne droite.
    C'est la forme la plus simple que l'on peut obtenir avec Lenia avec un kernel gaussien et une fonction de croissance gaussienne.

    \subsection{Wanderer}\label{subsec:wanderer}

    La forme Wanderer est une forme qui se déplace librement dans la grille, elle peut tourner sur elle-même et se déplacer dans toutes les directions ou parfois s'immobiliser avant de repartir.
    Cette forme variera selon son environnement, cela changera donc selon les différentes simulations.

    Voici à quoi ressemble la forme Wanderer dans notre simulation à un instant donné:

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.5\textwidth]{assets/wanderer}
        \caption{Wanderer}
        \label{fig:wanderer}
    \end{figure}

    \subsection{Geminium}\label{subsec:geminium}

    La forme Geminium est une forme allongée qui se déplace en diagonale.
    Si rien ne l'arrête, elle continuera à se déplacer en diagonale sans jamais s'arrêter.
    Sa forme allongée ne changera pas tout comme sa direction de déplacement, cette forme stable sera ainsi toujours la même peu importe la simulation.

    Voici à quoi ressemble la forme Geminium dans notre simulation à un instant donné:


    \begin{figure}[H]
        \centering
        \includegraphics[width=0.5\textwidth]{assets/geminium}
        \caption{Geminium}
        \label{fig:geminium}
    \end{figure}

\end{document}