# Installations des dépendances

```bash
pip install -r requirements.txt
```

# Lancement du programme

```bash
python main.py
```

# Organisation du projet

- Le fichier `main.py` contient le code principal du programme Lenia.
- Le fichier `requirements.txt` contient les dépendances du projet.
